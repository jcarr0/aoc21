import GHC.IO
import Data.Functor
import Control.Arrow ((&&&))

main :: IO ()
main = print =<< (readFile "input" <&> lines <&> summary)
  where
    summary :: [String] -> Int
    summary = length . filter id . uncurry (zipWith (<)) . andPrev . slidingSum . parse

    andPrev :: [Int] -> ([Int], [Int])
    andPrev = id &&& drop 1

    slidingSum :: [Int] -> [Int]
    slidingSum (x:y:z:xs) = (x+y+z:(slidingSum (y:z:xs)))
    slidingSum _ = []

    parse :: [String] -> [Int]
    parse = map read

