import GHC.IO
import Data.Functor
import Control.Arrow ((&&&))

main :: IO ()
main = print =<< (readFile "input" <&> lines <&> summary)
  where
    summary :: [String] -> Int
    summary = length . filter id . uncurry (zipWith (<)) . andPrev . parse

    andPrev :: [Int] -> ([Int], [Int])
    andPrev = id &&& drop 1

    parse :: [String] -> [Int]
    parse = map read

