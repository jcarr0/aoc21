use "files"

actor Main
  new create(env: Env) =>
    try
      let auth = (env.root as AmbientAuth)
      let path = FilePath(auth, "input")?
      match OpenFile(path)
      | let file: File =>
        let lines = FileLines(file)

        var horiz: U64 = 0
        var vert: U64 = 0

        for line in lines do
          let components: Array[String] val = (consume val line).split(" ")
          let command = components(0)?
          let value = components(1)?.u64()?
          match command
          | "forward" => horiz = horiz + value
          | "up" => vert = vert - value
          | "down" => vert = vert + value
          end
        end
        env.out.print((horiz*vert).string())
      else
        env.err.print("File open failed")
      end
    else
      env.err.print("Failed!")
    end
